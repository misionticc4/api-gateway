const { ApolloServer } = require('apollo-server');

const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const UsuariosAPI = require('./dataSources/usuarios_api');
const ReservaAPI = require('./dataSources/reserva_api');
const autenticacion = require('./utils/autenticacion');

const server = new ApolloServer({
    context: autenticacion,
    typeDefs,
    resolvers,
    dataSources: () => ({
        usuariosAPI: new UsuariosAPI(),
        reservaAPI: new ReservaAPI()
    }),
    introspection: true,
    playground: true
});

server.listen(process.env.PORT || 4000).then(({ url }) =>{
    console.log(`🚀 Servidor listo en ${url}`);
});