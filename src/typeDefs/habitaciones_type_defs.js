const { gql } = require('apollo-server');

const habitacionesTypeDefs =  gql `
    type Habitacion {
        nombre: String!
        region: String!
        descripcion: String!
        tipo: String!
        estado: Boolean!
        precio: Int!
    }

    input HabitacionInput {
        nombre: String!
        region: String!
        descripcion: String!
        tipo: String!
        estado: Boolean!
        precio: Int!
    }

    input HabitacionEdit {
        region: String!
        descripcion: String!
        tipo: String!
        estado: Boolean!
        precio: Int!
    }

    type DeleteHabitacion {
        nombre: String
    }

    extend type Mutation {
        crearHabitacion(habitacion: HabitacionInput!): Habitacion
        editarHabitacion(nombre: String!, habitacionEditada: HabitacionEdit!): Habitacion
        borrarHabitacion(nombre: String!): DeleteHabitacion
    }

    extend type Query {
        listaHabitaciones: [Habitacion]
        habitacionByNombre(nombre: String!): Habitacion
    }
`;

module.exports = habitacionesTypeDefs;