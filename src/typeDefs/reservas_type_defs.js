const { gql } = require('apollo-server');

const reservasTypeDefs = gql `
    type Reserva {
        id: String!
        username: String!
        nombreHabitacion: String!
        fechaIngreso: String!
        fechaSalida: String!
        fechaCreacionReserva: String!
        noches: Int!
        valorReserva: Int!
    }

    input ReservaInput {
        username: String!
        nombreHabitacion: String!
        fechaIngreso: String!
        fechaSalida: String!
    }

    type DeleteReserva {
        id: String
    }

    extend type Mutation {
        crearReserva(reserva: ReservaInput!): Reserva
        borrarReserva(id: String!): DeleteReserva

    }
    
    extend type Query {
        reservaByUsuario(username: String!): [Reserva]
    }
`;

module.exports = reservasTypeDefs;