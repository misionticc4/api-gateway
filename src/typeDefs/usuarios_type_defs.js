const { gql } = require('apollo-server');

const usuariosTypeDefs = gql `
    type Tokens {
        refresh: String!
        access: String!
    }

    type Access {
        access: String!
    }

    input CredentialsInput {
        username: String!
        password: String!
    }

    input SignUpInput {
        documento: String!
        username: String!
        password: String!
        email: String!
        telefono_celular: String
        nombres: String!
        apellidos: String!
    }

    type UserDetail {
        username: String!
        password: String!
        documento: String!
        email: String!
        nombres: String!
        apellidos: String!
    }

    type Mutation {
        signUpUser(userInput:SignUpInput): Tokens!
        logIn(credentials:CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
    }

    type Query {
        userDetailByDocumento(documento: String!): UserDetail!
    }

`;

module.exports = usuariosTypeDefs;