//Se llama al typedef (esquema) de cada submodulo
const usuariosTypeDefs = require("./usuarios_type_defs");
const habitacionesTypeDefs =  require("./habitaciones_type_defs");
const reservasTypeDefs = require("./reservas_type_defs");
//Se unen
const schemasArrays = [usuariosTypeDefs, habitacionesTypeDefs, reservasTypeDefs];
//Se exportan
module.exports = schemasArrays;