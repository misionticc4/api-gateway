const { RESTDataSource } = require('apollo-datasource-rest');

const serverConfig = require('../server');

class UsuariosAPI extends RESTDataSource {
    
    constructor() {
        super();
        this.baseURL = serverConfig.usuarios_api_url;
    }

    async createUsuario(usuario){
        usuario = new Object(JSON.parse(JSON.stringify(usuario)));
        return await this.post(`/user/`, usuario);
    }

    async getUsuario(documento) {
        return await this.get(`/user/${documento}/`);
    }

    async authRequest(credentials) {
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
        return await this.post(`/login/`, credentials);
    }

    async refreshToken(token) {
        token = new Object(JSON.parse(JSON.stringify( { refresh:token } )));
        return await this.post(`/refresh/`, token);
    }
}
module.exports = UsuariosAPI;