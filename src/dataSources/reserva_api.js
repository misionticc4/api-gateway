const { RESTDataSource } = require('apollo-datasource-rest');

const serverConfig = require('../server');

class ReservaAPI extends RESTDataSource {

    constructor() {
        super();
        this.baseURL = serverConfig.reservas_api_url;
    }

    async createHabitacion(habitacion) {
        habitacion = new Object(JSON.parse(JSON.stringify(habitacion)));
        return await this.post('/habitaciones/', habitacion);
    }

    async getListaHabitacion() {
        return await this.get('/habitaciones/');
    }

    async getHabitacion(nombre) {
        return await this.get(`/habitaciones/${nombre}`);
    }

    async putHabitacion(nombre, edicion) {
        return await this.put(`/habitaciones/${nombre}`, edicion);
    }

    async deleteHabitacion(nombre) {
        nombre = new Object(JSON.parse(JSON.stringify(nombre)));
        return await this.delete(`/habitaciones/${nombre}`);
    }

    async createReserva(reserva) {
        reserva = new Object(JSON.parse(JSON.stringify(reserva)));
        return await this.post(`/reservas/`, reserva);
    }

    async getReserva(username) {
        return await this.get(`/reservas/${username}`);
    }

    async deleteReserva(id) {
        id = new Object(JSON.parse(JSON.stringify(id)));
        return await this.delete(`/reservas/${id}`);
    }

}
module.exports = ReservaAPI;