const habitacionesResolver = {
    Query: {
        listaHabitaciones: (_, {  }, { dataSources }) =>
        dataSources.reservaAPI.getListaHabitacion(),

        habitacionByNombre: (_, { nombre }, { dataSources }) => 
            dataSources.reservaAPI.getHabitacion(nombre),
    },
    Mutation: {
        crearHabitacion: (_, { habitacion }, { dataSources }) => {
            const habitacionInput = {
                nombre: habitacion.nombre,
                region: habitacion.region,
                descripcion: habitacion.descripcion,
                tipo: habitacion.tipo,
                estado: habitacion.estado,
                precio: habitacion.precio,
            }
            return dataSources.reservaAPI.createHabitacion(habitacionInput);
        },
        editarHabitacion: (_, { nombre, habitacionEditada }, { dataSources }) => {
            const habitacionEdit = {
                region: habitacionEditada.region,
                descripcion: habitacionEditada.descripcion,
                tipo: habitacionEditada.tipo,
                estado: habitacionEditada.estado,
                precio: habitacionEditada.precio,
            }
            return dataSources.reservaAPI.putHabitacion(nombre, habitacionEdit);
        },
        borrarHabitacion: (_, { nombre }, { dataSources }) => 
            dataSources.reservaAPI.deleteHabitacion(nombre),
    }
};
module.exports = habitacionesResolver;