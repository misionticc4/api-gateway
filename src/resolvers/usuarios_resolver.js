const usuariosResolver = {
    Query: {
        userDetailByDocumento: (_, { documento }, { dataSources, documentoToken }) => {
            if (documento == documentoToken)
                return dataSources.usuariosAPI.getUsuario(documento);
            else
                return null
        },
    },

    Mutation: {
        signUpUser: (_, { userInput }, { dataSources }) => {
            const usuarioInput = {
                documento: userInput.documento,
                username: userInput.username,
                password: userInput.password,
                email: userInput.email,
                telefono_celular: userInput.telefono_celular,
                nombres: userInput.nombres,
                apellidos: userInput.apellidos,
            }
            return dataSources.usuariosAPI.createUsuario(usuarioInput);
        },
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.usuariosAPI.authRequest(credentials),

        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.usuariosAPI.refreshToken(refresh),
    }
};
module.exports = usuariosResolver;