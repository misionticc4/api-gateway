const reservasResolver = {
    Query: {
        reservaByUsuario: async(_, { username }, { dataSources, documentoToken }) => {
            usuarioToken = (await dataSources.usuariosAPI.getUsuario(documentoToken)).username
                if (username == usuarioToken)
                    return dataSources.reservaAPI.getReserva(username);
                else
                    return null
            //Transaction.usernameOrigin = usernameToken
        }
    },
    Mutation: {
        crearReserva: async(_, { reserva }, { dataSources, documentoToken }) => {
            usuarioToken = (await dataSources.usuariosAPI.getUsuario(documentoToken)).username
            if (reserva.username == usuarioToken) {
                const reservaInput = {
                    username: reserva.username,
                    nombreHabitacion: reserva.nombreHabitacion,
                    fechaIngreso: reserva.fechaIngreso,
                    fechaSalida: reserva.fechaSalida,
                    fechaCreacionReserva: (new Date()).toISOString()
                }
                return dataSources.reservaAPI.createReserva(reservaInput);
            }
            else
                return null
        },
        borrarReserva: async(_, { id }, { dataSources }) => 
            dataSources.reservaAPI.deleteReserva(id)

    }
};

module.exports = reservasResolver;