const usuariosResolver = require('./usuarios_resolver');
const habitacionesResolver = require('./habitaciones_resolver')
const reservasResolver = require('./reservas_resolver');

const lodash = require('lodash');

const resolvers = lodash.merge(usuariosResolver, habitacionesResolver, reservasResolver);

module.exports = resolvers;